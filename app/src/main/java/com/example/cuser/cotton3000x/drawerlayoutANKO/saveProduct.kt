package com.example.cuser.cotton3000x.drawerlayoutANKO

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import kotlin.coroutines.experimental.CoroutineContext

fun savePhotos(
        app: App,
        roducts: List<ProductInfo>,
        coroutineContext: CoroutineContext = CommonPool
): Deferred<Unit> = async(coroutineContext) {
    app.database.productDao().insertAll(roducts)
}