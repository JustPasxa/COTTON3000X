package com.example.cuser.cotton3000x.drawerlayoutANKO

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.CardView
import android.support.v7.widget.PopupMenu
import android.view.Gravity
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.example.cuser.cotton3000x.R
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.sdk25.coroutines.onClick


class ProductView(context: Context) : _LinearLayout(context) {

    fun bind(productInfo: ProductInfo){
        layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                dip(290))
        orientation = VERTICAL

        customView<CardView> {
            gravity = Gravity.CENTER
            cardElevation = 15f //5f
            radius = 15f
            preventCornerOverlap = false

            linearLayout {
                lparams(matchParent, wrapContent)
                orientation = VERTICAL
                imageView {
                    scaleType = ImageView.ScaleType.CENTER_CROP
                    id = R.id.image_card_view
                    backgroundResource = R.drawable.min_size_image

                    elevation = 50f
                }.lparams(matchParent, dip(230))

                linearLayout {
                    orientation = HORIZONTAL

                    relativeLayout {
                        linearLayout {
                            orientation = VERTICAL
                            textView(productInfo.name) {
                                id = R.id.text_news
                                textColor = Color.BLACK
                                textSize = 25f
                            }.lparams(wrapContent, dip(30))

                            textView("от " + productInfo.price) {
                                gravity = Gravity.BOTTOM
                            }.lparams(wrapContent, dip(20)) {
                                bottomMargin = dip(10)
                            }
                        }.lparams(wrapContent, matchParent) {
                            leftMargin = dip(10)
                            alignParentLeft()
                        }

                        imageButton {
                            id = R.id.image_product_button
                            bottomPadding = dip(50)
                            setImageResource(R.drawable.menu_two)
                            scaleX = 0.55f
                            scaleY = 0.6f
                            background = null
                            //backgroundColor = Color.BLACK
                            topPadding = dip(40)
                            rightPadding = dip(20)

                            onClick {
                                var popupMenu: PopupMenu? = null
                                popupMenu = PopupMenu(it!!.context, it)
                                popupMenu.inflate(R.menu.popup_product)
                                popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->
                                    when (item!!.itemId) {
                                        R.id.menu1 -> {
                                            Toast.makeText(it.context, productInfo.name + " дабавилась", Toast.LENGTH_LONG).show()
                                        }
                                    }
                                    true
                                })
                                popupMenu.show()
                            }

                        }.lparams(dip(30), dip(60)) {
                            alignParentRight()
                        }
                    }
                }.lparams(matchParent, dip(100))
            }
        }.lparams(matchParent, matchParent) {
            bottomMargin = dip(10)
            leftMargin = dip(3)
            rightMargin = dip(3)
        }
    }

}

