package com.example.cuser.cotton3000x.drawerlayoutANKO


import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [ProductInfo::class], version = 1)
abstract class CatalogDatabase : RoomDatabase() {

    abstract fun productDao(): ProductDao
}
