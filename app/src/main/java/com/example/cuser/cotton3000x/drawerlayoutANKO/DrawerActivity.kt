package com.example.cuser.cotton3000x.drawerlayoutANKO

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.LinearLayout.VERTICAL
import android.widget.RelativeLayout
import android.widget.Toast
import com.example.cuser.cotton3000x.R
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.find
import org.jetbrains.anko.findOptional
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.searchView
import org.jetbrains.anko.textView


class DrawerActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var draw : DrawUI

    private val productList = ProductInfo.List()
    private val reposAdapter = ProductListAdapter(productList) { repoName ->
        Toast.makeText(this, repoName, Toast.LENGTH_SHORT).show()
    }

    private lateinit var productV : RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        draw = customView{}
        setContentView(draw)
        val toolbar = findOptional<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, draw, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        draw.addDrawerListener(toggle)
        toggle.syncState()

        findOptional<NavigationView>(R.id.nav_bar)!!.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (draw.isDrawerOpen(GravityCompat.START)) {
            draw.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        find<RelativeLayout>(R.id.conteiner_toolbar).removeAllViews()
        find<LinearLayout>(R.id.linear_layout).removeAllViews()
        productList.clear()

        when (item.itemId) {
            R.id.nav_camera -> {
                find<LinearLayout>(R.id.linear_layout).apply {
                    textView("text")
                }
            }
            R.id.nav_gallery -> {
                linearLayoutManager = LinearLayoutManager(this)
                find<LinearLayout>(R.id.linear_layout).apply {
                    linearLayout {
                        id = R.id.linear_delete
                        orientation = VERTICAL

                        productV = recyclerView {
                            id = R.id.recycler_news
                            lparams(matchParent, matchParent)
                            adapter = reposAdapter
                            layoutManager = linearLayoutManager
                        }
                    }
                }
                find<RelativeLayout>(R.id.conteiner_toolbar).apply {
                    searchView()
                }

                launch(UI) {

                    val cachedPhotos = loadingPhotosFromCache(application as App).await()
                    if (cachedPhotos.isNotEmpty()) {
                        productList.addAll(cachedPhotos)
                        productV.adapter.notifyDataSetChanged()
                    } else {
                        val cloudPhotosJob = loadingDataFromCloud()
                        cloudPhotosJob.start()
                        val cloudPhotos = cloudPhotosJob.await()
                        savePhotos(application as App, cloudPhotos)
                        productList.addAll(cloudPhotos)
                        productV.adapter.notifyDataSetChanged()
                    }

                }
            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        draw.closeDrawer(GravityCompat.START)
        return true
    }
}