package com.example.cuser.cotton3000x.drawerlayoutANKO


import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface ProductDao {

    @Query("SELECT * FROM ProductInfo")
    fun getAll(): List<ProductInfo>

    @Insert
    fun insertAll(products: List<ProductInfo>)
}
