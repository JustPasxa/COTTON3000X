package com.example.cuser.cotton3000x.drawerlayoutANKO

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity
data class ProductInfo(
    val price: String,
    @PrimaryKey
    val id: String,
    val name: String
) {
    class List : ArrayList<ProductInfo>()
}