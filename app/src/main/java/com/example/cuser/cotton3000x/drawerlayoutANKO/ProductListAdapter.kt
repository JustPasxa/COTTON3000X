package com.example.cuser.cotton3000x.drawerlayoutANKO

import android.graphics.Color
import android.support.v7.widget.CardView
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.*
import android.widget.LinearLayout.*
import com.example.cuser.cotton3000x.R
import org.jetbrains.anko.dip
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.sdk25.coroutines.onClick

class ProductListAdapter(
            private val repos: ProductInfo.List,
            private val onItemClick: (String) -> Unit) : RecyclerView.Adapter<ProductListAdapter.ReposListViewHolder>() {

    class ReposListViewHolder(val view: ProductView) : RecyclerView.ViewHolder(view)
    

    override fun getItemCount() = repos.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReposListViewHolder {

        val view = ProductView(parent.context)
        val width = RecyclerView.LayoutParams.MATCH_PARENT
        val height = 400
        view.layoutParams = RecyclerView.LayoutParams(width, height)

        return ReposListViewHolder(view).apply {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val name = repos[position].name
                    onItemClick(name)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ReposListViewHolder, position: Int) {
        val productV = holder.view
        val product = repos[position]
        productV.bind(product)
        
    }
}
