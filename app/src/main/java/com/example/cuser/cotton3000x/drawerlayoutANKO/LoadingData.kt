package com.example.cuser.cotton3000x.drawerlayoutANKO

import com.google.gson.Gson
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import okhttp3.OkHttpClient
import okhttp3.Request
import kotlin.coroutines.experimental.CoroutineContext

fun loadingDataFromCloud(
        coroutineContext: CoroutineContext = CommonPool
): Deferred<List<ProductInfo>> = async(coroutineContext) {
    // Создать клиент для HTTP запросов
    val httpClient = OkHttpClient()

    // Создать запрос
    val request = Request.Builder()
            .url("https://api.myjson.com/bins/1exfmb")
            .build()

    httpClient.newCall(request).execute().use {
        Gson().fromJson(it.body()!!.string(), ProductInfo.List::class.java)
    }
}

fun loadingPhotosFromCache(
        app: App,
        coroutineContext: CoroutineContext = CommonPool
): Deferred<List<ProductInfo>> = async(coroutineContext) {
    app.database.productDao().getAll()
}